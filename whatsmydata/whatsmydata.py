#!/usr/bin/env python3

'''
BUGS:
[X] parsing results from manova from R: works when there's no differences (<0.05),
    but NOT when there are. (skrzynka.stats)

'''

import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

import pandas as pd
import re
import base64
from io import StringIO, BytesIO
import os
from skrzynka.skrzynka import Options
#from skrzynka import stats
import distribution_fitting as ft

# cache
from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)

#from isdev import *
from objects import *
from webapp_utils import table_insert_hyperlink, table_colour_numeric_values


### OPTIONS
# as - analyze series

options = {
    'as_fit_dist_num':True,
    'as_summarize_num': True,
    'as_summarize_cat': True,
    'as_compare_cat': True,
    }

op = Options(**options)



# globally
def numeric_results(df):
    ss = df.select_dtypes(include = ['float64', 'int'])
    if ss.shape[0] == 0:
        print('No numeric columns')
        res = None
        # TODO
    else:
        res = ss.describe()
        p = plt.figure()
        for c in ss:
            p = sns.distplot(ss[c], hist = False, axlabel=False)
        plt.close()
        plt.close('all')
        return ss, p

# analyse each column separately

def infer_coltype(series):
    if series.dtype == 'int' and len(series.drop_duplicates()) < 5:
        dt = 'factor'
    elif series.dtype == 'object':
        dt = 'factor'
    elif series.dtype == 'float':
        dt = 'float'
    elif series.dtype == 'int':
        dt = 'int'
    elif series.index in ['TIME', 'Time', 'time']:
        dt = 'timeseries'
    else:
        dt = series.dtype
    return str(dt)


def analyze_df(df):
    """
    float, int, bool, datetime64[ns] and datetime64[ns, tz] (in >= 0.17.0),
    timedelta[ns], category (in >= 0.15.0), and object. In addition these dtypes
    have item sizes, e.g. int64 and int32.
    """
    #TODO also chech index for information
    df.drop(df.columns[df.columns.str.contains("Unnamed: ")],axis=1,inplace=True)
    results = {}

    dts = [(i, infer_coltype(j)) for i,j in df.iteritems()]
    for cn, dt in dts:
        print("-"*80, "\n", cn, "\n", "-"*80)
        if dt in ['float', 'int']:
            results[cn] = analyze_df_numeric(cn, df)
        elif dt in ['factor']:
            #if is just  ['str']:
            # 'factor': if str but value_counts > df.shape[0]
            #results[cn] = analyze_df_string(cn, df)
            results[cn] = analyze_df_factor(cn, df)
        else:
            results[cn] = analyze_df_unknown(cn,df)
    return results

def analyze_df_unknown(cn,df):
    texts =            ['Column name: '+str(cn), 'Column type: '+str(df[cn].dtypes), 'Data type not understood']
    titles_texts =     ['', 'Column information']
    plots =            []
    titles_plots =     []
    descr_plots =      []
    tables =           []
    titles_tables =    []
    descr_tables =     []

    return {
            'texts':            texts,
            'titles_texts':     titles_texts,
            'plots':            plots,
            'titles_plots':     titles_plots,
            'descr_plots':      descr_plots,
            'tables':           tables,
            'titles_tables':    titles_tables,
            'descr_tables':     descr_tables,
            }


def analyze_df_factor(cn,df):
    data = df[cn]

    texts =            ['Column name: '+str(cn), 'Column type: '+str(df[cn].dtypes)]
    titles_texts =     ['', 'Column information']
    plots =            []
    titles_plots =     ['']
    descr_plots =      ['']
    tables =           []
    titles_tables =    ['']
    descr_tables =     ['']

    if op.as_summarize_cat:
        summary = df.groupby(cn).describe()#.melt()
        summary.reset_index(inplace=True)
        tables.append(summary)
        titles_tables.append('Table: Summary by '+str(cn))
        descr_tables.append('')

    if op.as_compare_cat:
        #TODO for each numeric
        # perform manova which I think is applicable for two samples and one variable too?
        try:
            df.to_csv(uploads_dir+'tmpfile.tsv', sep='\t')
            formula = 'cbind('+\
                ','.join(df.columns[[c in ['float', 'int'] for c in df.dtypes]])+\
                ")~"+str(cn)

            #FIXME: replace woth a python implementation
            #manova_res = stats.manovaR(uploads_dir+'tmpfile.tsv', formula = formula, groups='all')
            #manova_res = stats.manovaR_results2table(manova_res)

            #manova_res = [table_colour_numeric_values(t, colname='Df') for t in manova_res]

            #manova_titles = ["Titles: MANOVA results for all variables"]+\
            #        ["Titles: MANOVA results for response"+\
            #        str(tt.columns[0]).replace("Response ", " ") for tt in manova_res[1:]]
            #manova_res = [t.drop(t.columns[0], axis=1) for t in manova_res]


            #[titles_tables.append(t) for t in manova_titles]
            #[tables.append(t) for t in manova_res]

            [titles_tables.append(t) for t in []]
            [tables.append(t) for t in []]
            texts.append("Problem: Couldn't calculate MANOVA. (not implemented)")

        except: #rpy2.rinterface.RRuntimeError:
            texts.append("Problem: Couldn't calculate MANOVA.")

    return {
            'texts':            texts,
            'titles_texts':     titles_texts,
            'plots':            plots,
            'titles_plots':     titles_plots,
            'descr_plots':      descr_plots,
            'tables':           tables,
            'titles_tables':    titles_tables,
            'descr_tables':     descr_tables,
            }



def analyze_df_numeric(cn, df):
    data = df[cn]

    texts =            ['Column name: '+str(cn), 'Column type: '+str(df[cn].dtypes)]
    titles_texts =     ['', 'Column information']
    plots =            []
    titles_plots =     ['']
    descr_plots =      ['']
    tables =           []
    titles_tables =    ['']
    descr_tables =     ['']

    if op.as_summarize_num:
        summary = data.describe()
        tables.append(summary)
        titles_tables.append('Table: Column summary')

    #p = plt.figure()
    #p = sns.distplot(df[cn], hist = False, axlabel=False)

    if op.as_fit_dist_num:
        # Plot for comparison
        img = BytesIO()

        p_all_dist = plt.figure(figsize=(12,8))
        ax = data.plot(kind='hist', bins=50, density=True, alpha=0.5, color='blue')
            #color=plt.rcParams['axes.color_cycle'][1])
        # Save plot limits
        dataYLim = ax.get_ylim()

        # Find best fit distribution
        best_fit_name, best_fit_params, all_fits = ft.best_fit_distribution(data, 200, ax)
        best_dist = getattr(ft.st, best_fit_name)

        # Update plots
        ax.set_ylim(dataYLim)
        ax.set_title(u'All fitted distributions')
        ax.set_xlabel(cn)
        ax.set_ylabel('Frequency')

        plt.savefig(img)
        p_all_dist_url = base64.b64encode(img.getvalue()).decode()
        plt.close()
        plt.close('all')

        img = BytesIO()
        # Make PDF
        pdf = ft.make_pdf(best_dist, best_fit_params)

        #plt.close()
        # Display
        p_best_dist = plt.figure(figsize=(12,8))
        ax = pdf.plot(lw=2, label='PDF', legend=True)
        data.plot(kind='hist', bins=50, density=True, alpha=0.5, label='Data',
                legend=True, ax=ax, color='blue')

        param_names = (best_dist.shapes + ', loc, scale').split(', ') if best_dist.shapes else ['loc', 'scale']
        param_str = ', '.join(['{}={:0.2f}'.format(k,v) for k,v in zip(param_names, best_fit_params)])
        dist_str = '{}({})'.format(best_fit_name, param_str)

        ax.set_title(u'Best fit distribution\n' + dist_str)
        ax.set_xlabel(cn)
        ax.set_ylabel('Frequency')

        plt.savefig(img)
        p_best_dist_url = base64.b64encode(img.getvalue()).decode()
        plt.close()
        plt.close('all')
        plots.append(p_all_dist_url)
        #titles_plots.append('Column summary')
        plots.append(p_best_dist_url)
        #titles_plots.append('Column summary')

        #tables.append(pd.DataFrame(all_fits, index=['params', 'sse']))
        all_fits_table = pd.DataFrame(all_fits).T.reset_index()
        all_fits_table.columns = ['distribution', 'parameters', 'SSE']
        #FIXME
        #all_fits_table = table_insert_hyperlink(all_fits_table,
        #                    colname='distribution',
        #                    url_template = URL_TEMPLATES['distribution'])
        tables.append(all_fits_table)
        titles_tables.append('Table: Fitted distributions with best parameters and residual sum of squares')

    return {
            'texts':            texts,
            'titles_texts':     titles_texts,
            'plots':            plots,
            'titles_plots':     titles_plots,
            'descr_plots':      descr_plots,
            'tables':           tables,
            'titles_tables':    titles_tables,
            'descr_tables':     descr_tables,
            }

def analyze_df_factor_breakdown(df, cn):
    '''
    When the user clicks on a link next to the factor column name
    Redirect to this column's page
    '''
    texts =            ['Column name: '+str(cn), 'Column type: '+str(df[cn].dtypes)]
    titles_texts =     ['', 'Column information']
    plots =            []
    titles_plots =     ['']
    descr_plots =      ['']
    tables =           []
    titles_tables =    ['']
    descr_tables =     ['']

    for level in df[cn].drop_duplicates().tolist():
        res = analyze_df_numeric(cn, df[df[cn] == level])

        yield res

    #return {
    #        'texts':            texts,
    #        'titles_texts':     titles_texts,
    #        'plots':            plots,
    #        'titles_plots':     titles_plots,
    #        'descr_plots':      descr_plots,
    #        'tables':           tables,
    #        'titles_tables':    titles_tables,
    #        'descr_tables':     descr_tables,
    #        }
