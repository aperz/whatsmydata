#!/usr/bin/env python3

'''
TODOs:
'''


from flask import *
import matplotlib.pyplot as plt
import base64
import pandas as pd
import numpy as np
import re
from io import StringIO, BytesIO
import datetime
import inspect
import os

from skrzynka import flatten, names_to_human_readable
from objects import *

#from errch_url import * # functions for each module that call the respective make_user_table fun will import
app = Flask(__name__)

# tables
pd.set_option('display.max_colwidth', 1000)

# cache
from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)

def analyze_df2html(results):
    html_results = results
    for cn, res in html_results.items():
        for obj_type, objs in res.items():
            #if obj_type == 'plots':
            #    html_results[cn][obj_type] = [plot2html(o) for o in objs]
            if obj_type == 'tables':
                html_results[cn][obj_type] = [table2html(o, reset_index=False)\
                                                for o in objs]
    return html_results

def plot2html(plot_func, **kwargs):
    img = BytesIO()

    plot_func(**kwargs)

    plt.savefig(img)
    plot_url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    plt.close('all')
    return plot_url


def file2table(fp):
    # TODO infer type
    #if not os.path.splitext(fp)[-1] in [".txt", ".csv", ".tsv", ".xls", ".xlsx"]:
    #    raise TypeError('Filetype not supported.')
    #else:
    #    if os.path.splitext(fp)[-1] in [".xls", ".xlsx"]:
    #        #table = pd.ExcelFile(fp).parse(0) #first sheet
    #        #FIXME
    #        table = pd.ExcelFile(fp).parse(5)
    #    if os.path.splitext(fp)[-1] == ".csv":
    #        table = pd.read_csv(fp, sep=',')#, index_col=0)
    #    if os.path.splitext(fp)[-1] == ".tsv":
    #        table = pd.read_csv(fp, sep='\t')#, index_col=0)
    #    if os.path.splitext(fp)[-1] in [".xls"]:
    #        table = pd.read_excel(fp)
    #    return table

    # TODO does pandas have a file inspection tool? How to elegantly determine what the file is?
    # file type, encoding, ...


    try:
        tablec = pd.read_csv(fp, sep=',')#, index_col=0)
        tablet = pd.read_csv(fp, sep='\t')#, index_col=0)


        if tablet.shape[1] > tablec.shape[1]:
            table = tablet
        else:
            table = tablec

        if table.shape[0] ==0 or table.shape[1] ==0:
            pass
        else:
            return table
    except:
        pass

    try:
        tablec = pd.read_csv(fp, sep=',', encoding='latin-1')#, index_col=0)
        tablet = pd.read_csv(fp, sep='\t', encoding='latin-1')#, index_col=0)


        if tablet.shape[1] > tablec.shape[1]:
            table = tablet
        else:
            table = tablec

        if table.shape[0] ==0 or table.shape[1] ==0:
            pass
        else:
            return table
    except:
        pass

    try:
        table = pd.read_csv(fp, sep='\t')#, index_col=0)
        if table.shape[0] ==0 or table.shape[1] ==0:
            pass
        else:
            return table
    except:
        pass

    try:
        #table = pd.ExcelFile(fp).parse(0) #first sheet
        #FIXME
        table = pd.ExcelFile(fp).parse(5)
        if table.shape[0] ==0 or table.shape[1] ==0:
            pass
        else:
            return table
    except:
        pass


    try:
        table = pd.read_excel(fp)
        if table.shape[0] ==0 or table.shape[1] ==0:
            pass
        else:
            return table
    except:
        pass

    raise TypeError('Filetype not supported.')



def render_template_defaults(template_name_or_list, **kwargs):
    return render_template(template_name_or_list = template_name_or_list,
                    TITLE = TITLE,
                    SUFFIX=SUFFIX,
                    REDIR_INTRO = REDIR_INTRO,
                    REDIR_HOME = REDIR_HOME,
                    **kwargs
                    )
    # TODO store WA globals in a dict
    # DONT DO GLOBALS

def render_template_with_header(template_name_or_list, **kwargs):
    return render_template_defaults('header.html') + \
                render_template_defaults(template_name_or_list = template_name_or_list,
                    **kwargs
                    )


def usage_message(query):
    message = "="*20 + "\n\n" +\
        str(datetime.datetime.now()) + "\n" +\
        str(request.remote_addr) + "\n" +\
        str((inspect.stack()[1][3])) + "\n" +\
        query + "\n\n" + "="*20
    return message


def table_insert_hyperlink(d, url_template, colname='use_column_names'):
    #FIXME - needs some debugging
    d_ = d.copy()
    colname = flatten(d_.columns.str.findall(colname, flags=re.I))
    print(colname)
    if len(colname) == 1:
        colname = colname[0]
        s = '<a href="'+url_template+'">{0}</a>'
        d_[colname] = d_[colname].apply(lambda x: s.format(x))
        return d_
    else:
        raise ValueError ('Colname not found or is not specific.')

def table_colour_numeric_values(d, colname):
    def check_cond(x):
        if isinstance(x, float):
            return x<20
        else:
            return False

    #colname = flatten(d.columns.str.findall(colname, flags=re.I))
    #if len(colname) == 1:
        #colname = colname[0]
    d_ = d.copy()
    s = '<color="red">{0}</color>'
    d_[colname] = d_[colname].apply(lambda x: s.format(x))# if check_cond(x) else x)
    return d_
    #else:
    #    raise ValueError ('Colname not found or is not specific.')

def table2html(d, reset_index=True, index_col_name = None, **kwargs):
    '''
    d           A pandas DataFrame
    index_col_name  Name for the new column made from the index.
                If not provided, the name of index is used
    **kwargs    Other args to be passed to to_html()

    Returns:    A formatted string
    '''
    if isinstance(d, pd.core.series.Series):
        d = pd.DataFrame(d).T

    if reset_index:
        if index_col_name == None:
            if d.index.name == None:
                d.index.name = 'column name'
        else:
            d.index.name = index_col_name

        if not d.index.name in d.columns:
            d.insert(0, d.index.name, d.index) #names_to_human_readable(d.index))

    #if not d.shape[0] == 0:
        #d = d.ix[:min(max_rows, d.shape[0]), :]

    #d.columns = names_to_human_readable(d.columns)
    d = d.to_html(index = False, escape=False, **kwargs)
    return d

