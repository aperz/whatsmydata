#!/usr/bin/env python3

import pandas as pd
import re


# cache
from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)

# globals
OUTPUTS_DIR = 'outputs'

global STORAGE
STORAGE = {}
#STORAGE['sample_text'] = TEXT.sample_text #ATT ISDEV

from whatsmydata import distribution_fitting
