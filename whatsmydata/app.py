#!/usr/bin/env python3

'''
'''
TODOS = """
        [ ] parallelize col processing
        ---
        [x] make a demo
        ---
        [ ] QC type thing: sample size is small-apropriate-big etc
        ---
        [ ] suggest stats tests
        ---
        [ ] for each column, provide:
        <br>
         - a very basic summary incl. an inferred data type + a possibility to select a different type
        <br>
         - a link to a more thorough analysis page
        ---""".split("---")[:-1] # ?? last elt of list is empty? Well nvmd


import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import seaborn as sns

from flask import Flask
from flask import send_file, request
import pandas as pd
import re
from io import StringIO, BytesIO
import datetime
from werkzeug import secure_filename

from skrzynka import dummy_df
import whatsmydata as wmd

import text_blocks
from webapp_utils import *

# tables
pd.set_option('display.max_colwidth', 1000)

# cache
from tempfile import mkdtemp
from joblib import Memory
location = mkdtemp()
memory = Memory(location='cache', verbose=0)

from isdev import *
from objects import *

app = Flask(__name__)

global STORAGE
STORAGE = {}

def format_external_links(entity_id, entity_label):
    return [i.format(entity_id) for i in get_entity_info(entity_label)['external_links_template']]


# VIEWS
# =====

@app.route('/test', methods=['POST', 'GET'])
def test():
    return 'test'

@app.route('/intro', methods = ['POST', 'GET'])
def intro():
    return 'TODO: INTRO'

@app.route('/todo', methods = ['POST', 'GET'])
def todo():
    return render_template_with_header('item_list.html',
                    page_title="TODO",
                    text = TODOS)


@app.route('/about', methods = ['POST', 'GET'])
def about():
    return 'TODO: ABOUT'

def list_files_indir(fp):
    #import urllib
    files = os.listdir(fp)
    url_template = "/"+fp+"/{0}"
    url_list = ['<a href="'+url_template.format(i)+'">{0}</a>'.format(i)
                    for i in files]
    #url_list = [url_template.format(i) for i in files]
    print(url_list)
    return files, url_list

@app.route('/sample', methods = ['POST', 'GET'])
def sample():
    #'mnemo_contrasts':      '/contrasts/{0}',
    #FIXME
    files, file_urls = list_files_indir("sample_data")

    return render_template_with_header('download_files.html',
                                    files=file_urls,
                                    #dloads = files,
                                    #dloads_src = file_paths,
                                    )


@app.route('/', methods=['POST', 'GET'])
def home():
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    return render_template_with_header('home.html',
                        page_title              = TITLE,
                        text_introduction       = text_blocks.introduction,
                        sample_input            = text_blocks.sample_input,
                        )

@app.route('/upload_file', methods = ['GET', 'POST'])
def upload_file():
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    if not request.method == 'POST':
        print("=== Something went wrong.")
        return render_template_with_header('simple_text.html',
                    text = ['Something went wrong.']
                    )
    else:
        file_id = str(np.random.randint(1000000000, 9999999999)) #FIXME (no extension info)
        f = request.files['file']
        #TODO periodically clean the uploads folder !!!
        #TODO check if filename has appropriate extension
        #f.save(uploads_dir+secure_filename(f.filename))
        f.save(uploads_dir+secure_filename(file_id))

        redirect_url = url_for('analysis_results_browser',file_id=file_id)
        return redirect(redirect_url)

@app.route("/paste_text", methods=['POST', 'GET'])
def paste_text():
    return "NI"
    #TODO save as a file with a new file id (table id)

    print("="*19 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    if not request.method == 'POST':
        print("=== Something went wrong.")
        return render_template_with_header('simple_text.html',
                    text = ['Something went wrong.']
                    )
    else:
        text = request.form['text']
        return analysis_results_browser(text)


@app.route("/analysis_results/dataset/<file_id>", methods=['POST', 'GET'])
def analysis_results_browser(file_id):
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)
    # this will need to use cookies at some point
    #if not request.method == 'POST': # is not POST anymore
    #    print("=== Something went wrong.")
    #    return render_template_with_header('simple_text.html',
    #                text = ['Something went wrong. Please, upload your file again.']
    #                )
    #try:
    #FIXME it's kinda stupid to base this on file name
    if file_id.find('DEMO_') == 0:
        table = file2table(data_dir+"datasets/"+secure_filename(file_id))
    else:
        table = file2table(uploads_dir+secure_filename(file_id))
    print(table)
    print(table.dtypes)

    results = wmd.analyze_df(table)
    results = analyze_df2html(results)

    #column_ids = {cn:i for i,cn in enumerate(table.columns)}
    #links = {cn:url_for('column_page', column_id = cid) for cn,cid in column_ids.items()}
    links = {cn:[url_for('column_page',file_id=file_id, column_id=cn)] for cid,cn in enumerate(table.columns)}

    view = render_template_defaults('header.html') + \
            ''.join(render_template_defaults('results_page.html',
                        page_title=cn, links = links[cn], **results[cn])
            for cn in results.keys())

    return view

    #except TypeError:
    #    return render_template_with_header('simple_text.html',
    #                #text = ['Seems like your file type or structure is not supported. Please provide a simple table.']
    #                text = ['Something went wrong.']
    #                )


@app.route("/analysis_results/dataset/<file_id>/column_page/<column_id>", methods=['POST', 'GET'])
def column_page(file_id, column_id):
    return render_template_with_header('simple_text.html',
                text = ["Not implemented yet."]
                )
    try:
        table = file2table(uploads_dir+secure_filename(file_id))
        #table = STORAGE['input_table']
    except KeyError:
        #print("=== Couldn't find an item in STORAGE.")
        print("=== Couldn't find file ID.")
        return render_template_with_header('simple_text.html',
                    text = ["My bad, this should work - I'll fix this! For now, please, upload your file again."]
                    )

    #TODO how to pass the data?
    print("="*20 + "\n\n" + str(request.remote_addr) + "\n" + "="*20)

    view = ''
    results = wmd.analyze_df_factor(table, column_id)
    view = view + render_template_defaults('results_page.html',
                                page_title="Analyze column", links = [], **results)
    ##for i in wmd.analyze_df_factor_breakdown(table, column_id):
    #    print(i.shape)
    #    if i.shape > 0:
    #        view = view + render_template_defaults('results_page.html',
    #                            page_title="Analyze column", links = [], **results)

    return view



@memory.cache
def initiate():
    pass

if __name__ == "__main__":
    #PORT    = 9001
    app.run(host=HOST, port=PORT, debug=ISDEV)

