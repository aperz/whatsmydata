from isdev import *
if ISDEV:
    TITLE   = "What's My Data [DEV]"
    PORT    = 5003
    #HOST    = 'localhost'
    HOST    = '0.0.0.0'
    SUFFIX  = "DEV"
else:
    TITLE   = "What's My Data"
    PORT    = 9003
    HOST    = '0.0.0.0'
    SUFFIX  = ""

REDIR_HOME = "home"
REDIR_INTRO = "intro"
#figdir = '../figures/'
#resdir = '../results/'
data_dir = 'data/'
uploads_dir = 'uploads/'

from os.path import isdir
from os import mkdir
for d in [data_dir, uploads_dir]:
    if not isdir(d):
        mkdir(d)


# ===============


# black as placeholder
COLOURS = {
    'sample':               'green',
    'project':              'blue',
    'taxon':                'red',
    'biome':                'yellow',
    'function':             'orange',
    'condition':            'purple',
}

URL_TEMPLATES = {
    'term':             '/term/{0}',
    'distribution':    'https://docs.scipy.org/doc/scipy/reference/tutorial/stats/continuous_{0}.html',

}
