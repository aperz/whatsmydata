#!/usr/bin/env python3

NOTES = """
REFERENCES
http://www.statisticssolutions.com/data-levels-of-measurement/
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3116565/

TODO
- Focus on nominal and interval?
- object / dict:
    all combinations of independent variable and response var and question type
    + appropirate tests for each combination
    ? What with multivariate? (Leave for now)

PANDAS DTYPES
    float, int, bool, category, object,
    datetime64[ns] and datetime64[ns, tz], timedelta[ns]
    In addition these dtypes have item sizes, e.g. int64 and int32.

DATA LEVELS OF MEASUREMENT
nominal (categorical), e.g. Republican, Democrat, ...
    - pandas dtypes: object, category, bool
ordinal (levels / ranks): rank-ordered e.g. small, medium, learge, ...
    - pandas dtypes: object
interval (continuous), e.g. 1 to 2 is same interval as 89 to 90
    - pandas dtypes: float (int?)
ratio (scale), e.g. day 0, day1, ...
    - pandas dtypes: int, datetime64[ns], datetime64[ns, tz], timedelta[ns]

QUESTION TYPES
'difference'
    - interval or ratio-level Y variables, and categorical-level X variables
    - “Are there difference on the variable Y by variable X?”
        ANOVA
    - “Are there difference on the variable Y by variable X?” or, "Are there differences  on variables Y1, Y2, and Y3 by variables X1 and X2?"
        MANOVA
'relationship'
    - interval, ordinal-level, or ratio-level variables
        Spearman or Pearson correlations
    - dichotomous, categorical variable and an interval or ratio-level variable
        point-biserial correlation
    - two categorical variables
        chi-square
'prediction'
    - outcome variables are interval, ordinal, or categorical-level variables, respectively
        linear, ordinal, or multinomial regressions
        (independent variables can be interval/ordinal level variables or categorical-level variables)

POWER ANALYSIS
    - chosen based on the test
    - power: usually desired is .8
    - effect size: usually medium/large
    - alpha: usually .05
    - basic rules of thumb are 26 (for a large effect size) and 65 (for a moderate effect size)
        participants per group for a t-test, chi-square, correlation or linear regression with two predictors.

"""

import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

import pandas as pd
import numpy as np
import re
import base64
import os
from io import StringIO, BytesIO
#from skrzynka import Options

# cache
from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)


disambiguate = {
    'nominal':      'nominal',
    'categorical':  'nominal',
    'ordinal':      'ordinal',
    'levels':       'ordinal',
    'level':        'ordinal',
    'ranks':        'ordinal',
    'rank':         'ordinal',
    'interval':     'interval',
    'continuous':   'interval',
    'ratio':        'ratio',
    'scale':        'ratio',
    }

# DATA LEVELS OF MEASUREMENT
MEASUREMENT_LEVELS = {
    'nominal':  ['object', 'category', 'bool'],
    'ordinal':  ['object'],
    'interval': ['float' 'int'], #int?
    'ratio':    ['int', 'datetime64[ns]', 'datetime64[ns, tz]', 'timedelta[ns]'],
    }

QUESTION_TYPES = ['difference', 'relationship', 'prediction',]
# association' : 'relationship'


def which_test(question, independent_variable, response_variable,
            difference_n_categories=None, difference_paired = None,
            difference_parametric=None,
            relationship_n_categories=None, relationship_parametric=None,
            ):
    '''
    Help

    Chose parametric if you make assumptions regarding the normality of the underlying
    distribution

    ---
    Notes

    t-test etc | distribution
    relationship covered
    difference: independent can't really be interval (has to be binned)
                and response can't be nominal nor ordinal (?); covered (?)
    prediction: what about ratio? (day0, day1 - treated as interval?)
    '''
    assert question in QUESTION_TYPES
    assert independent_variable in MEASUREMENT_LEVELS.keys()
    assert response_variable in MEASUREMENT_LEVELS.keys()

    ### DEFAULTS
    if difference_parametric == None:
        difference_parametric = 'parametric'
    if relationship_parametric == None:
        relationship_parametric = 'parametric'

    t = 'No test was matched to data.'
    ###

    if question == 'difference':
        #print('question: difference')
        if difference_paired == 'unpaired':
            if independent_variable == 'nominal' and response_variable in ['interval', 'ratio']:
                # common error: repeatedly applying t-test (or its non-parametric counterpart,
                # Mann-Whitney U test -> increases the risk of incorrectly rejecting the null

                #print('variables: nominal, numeric')
                if difference_n_categories == '2':
                    if difference_parametric == 'parametric':
                        t = "t test."
                    elif difference_parametric == 'non-parametric':
                        t = "Mann-Whitney U test or Wilcoxon's rank sum test."
                    else:
                        raise TypeError("Define parameter: difference_parametric = 'parametric' or 'non-parametric'")

                elif difference_n_categories == '>2':
                    if difference_parametric == 'parametric':
                        t = """ANOVA (for multiple variables: MANOVA) or F test
                        (use a post-hoc test to determine which categories are different:
                        post-hoc: Tukey's Honestly Significant Difference test (Tukey-Kramer test), Bonferroni test, etc.)
                        """
                    elif difference_parametric == 'non-parametric':
                        t = """Kruskall-Wallis H test (Kruskall-Wallis ANOVA)
                        (use a post-hoc test to determine which categories are different:
                        post-hoc: Dunn's test.)
                        """
                    else:
                        raise TypeError("Define parameter: difference_parametric = 'parametric' or 'non-parametric'")

                else:
                    raise TypeError("Define number of categories of the independent variable (difference_n_categories = '2' or '>2')")


            elif independent_variable == 'nominal' and response_variable in ['nominal']:
                if difference_n_categories == '2':
                    t = "Chi-square test or Fisher's exact test"

                elif difference_n_categories == '>2':
                    t = "Chi-square test"
                else:
                    raise TypeError("Define number of categories of the independent variable (difference_n_categories = '2' or '>2')")


            elif difference_paired == 'paired':
                if independent_variable == 'nominal' and response_variable in ['interval', 'ratio']:
                    if difference_n_categories == '2':
                        if difference_parametric == 'parametric':
                            t = "Paired t-test"
                        elif difference_parametric == 'non-parametric':
                            t = "Wilcoxon's matched pairs signed rank test"
                        else:
                            raise TypeError("Define parameter: difference_parametric = 'parametric' or 'non-parametric'")

                    elif difference_n_categories == '>2':
                        if difference_parametric == 'parametric':
                            t = """Repeated measures ANOVA
                            (use a post-hoc test to determine which categories are different:
                            post-hoc: Wilcoxon's matched pairs signed rank test.)
                            """
                        elif difference_parametric == 'non-parametric':
                            t = """Friedman's ANOVA
                            (use a post-hoc test to determine which categories are different:
                            post-hoc: Dunn's test.)
                            """
                        else:
                            raise TypeError("Define parameter: difference_parametric = 'parametric' or 'non-parametric'")
                    else:
                        raise TypeError("Define number of categories of the independent variable (difference_n_categories = '2' or '>2')")

                elif independent_variable == 'nominal' and response_variable in ['nominal']:
                    if difference_n_categories == '2':
                        t = "McNemar's test (or its exact variants)"

                    elif difference_n_categories == '>2':
                        t = "Cochran's Q test"
                else:
                    raise TypeError("Define number of categories of the independent variable (difference_n_categories = '2' or '>2')")
        else:
            raise TypeError("Define argument: difference_paired = 'paired' or 'unpaired'")

    elif question == 'relationship':
        if independent_variable in ['interval', 'ordinal', 'ratio']\
            and response_variable in ['interval', 'ordinal', 'ratio']:
            if relationship_parametric == 'parametric':
                t = "Pearson's product moment correlation coefficient"
            elif relationship_parametric == 'non-parametric':
                t = "Spearman's or Kendall's rank correlation coefficient"
            else:
                raise TypeError("Define parameter: relationship_parametric = 'parametric' or 'non-parametric'")

        elif independent_variable in ['nominal']\
                and response_variable in ['interval', 'ordinal', 'ratio']:
            t = 'Point-biserial correlation (if number of categories = 2)'

        elif independent_variable in ['nominal']\
                and response_variable in ['nominal']:
            if relationship_n_categories == '2':
                t = "Relative risk (risk ratio) or odds ratio"
            elif relationship_n_categories == '>2':
                t = "Chi-square test for trend or logistic regression"
            else:
                raise TypeError("Define number of categories (difference_n_categories = '>2' (if any of the variables have more that 2 categories) or '2')")
        else:
            t = 'No test was matched to data.'

    elif question == 'prediction':
        if independent_variable in ['interval', 'ordinal', 'nominal']\
            and response_variable in ['interval']:
            t = 'linear regression'
        elif independent_variable in ['interval', 'ordinal', 'nominal']\
            and response_variable in ['ordinal']:
            t = 'ordinal regression'
        elif independent_variable in ['interval', 'ordinal', 'nominal']\
            and response_variable in ['nominal']:
            t = 'multinomial regression'

    return re.sub("\n {2,}", " ", t).strip(" ")


